package com.sda.decorator;

abstract class AbstractAccountDecorator extends AbstractAccount {
    AbstractAccount decoratedAccount;

    public AbstractAccountDecorator(AbstractAccount decoratedAccount) {
        this.decoratedAccount = decoratedAccount;
    }

    public void verifyAccount() {
    }
}

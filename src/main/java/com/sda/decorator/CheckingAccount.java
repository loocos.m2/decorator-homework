package com.sda.decorator;

public class CheckingAccount extends AbstractAccount {
    private double overdraft;

    public void verifyAccount() {
        overdraft = -50;
        if (this.balance < overdraft) {
            System.out.println("Overdraft Limit exceed!");
        }
    }
}

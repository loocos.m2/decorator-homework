package com.sda.decorator;

abstract class AbstractAccount {
    double balance;

    public abstract void verifyAccount();
}

package com.sda.decorator;

public class SmsManageableAccountDecorator extends AbstractAccountDecorator {

    public SmsManageableAccountDecorator(AbstractAccount decoratedAccount) {
        super(decoratedAccount);
    }

    @Override
    public void verifyAccount() {
        sendSmsToCustomer();
        super.verifyAccount();
    }

    private void sendSmsToCustomer() {
    }
}
